-- Script of caves/smith_cave.
local map = ...
local game = map:get_game()

local sword_price = 75


function map:on_started()
  smith:start_hammering()
end


function smith:start_hammering()
  local sprite = smith:get_sprite()
  sprite:set_direction(0)
  sprite:set_animation"hammering"
end

function smith:stop_hammering()
  local sprite = smith:get_sprite()
  sprite:set_animation"stopped"
end

-- Interaction with smith.
function smith:on_interaction()
  smith:stop_hammering()
  -- Smith dialog.
  if not game:get_value("player_has_sword") or not game:has_item("sword") then
    game:start_dialog("smith_cave.without_sword", function(answer)

      -- Ask if player wants to buy sword.
      if answer == 2 then
        -- Player does not want to buy sword.
        game:start_dialog("smith_cave.not_buying")
        smith:start_hammering()
      else
        -- Player wants to buy sword, check if money available.
        if game:get_money() < sword_price then
          -- Not enough money.
          sol.audio.play_sound("wrong")
          game:start_dialog("smith_cave.not_enough_money", function()
            smith:start_hammering()
          end)
        else
          -- Buy sword.
          game:remove_money(sword_price)
          sol.audio.play_sound("treasure")
          hero:start_treasure("sword", 1, "player_has_sword", function()
            game:start_dialog("smith_cave.thank_you", function()
              smith:start_hammering()
            end)
          end)
        end
      end
    end)
  else
    -- Player has sword. Go no further.
    game:start_dialog("smith_cave.with_sword", function()
      smith:start_hammering()
    end)
  end
end

function chest:on_opened()
  sol.audio.play_sound("wrong")
  game:start_dialog("_empty_chest", function()
    hero:unfreeze()
  end)
end
