-- Initialize destructible behavior specific to this quest.

local destructible_meta = sol.main.get_metatable("destructible")

-- TODO: Add more sprites if necessary
local item_drop_sprites = {
  "destructibles/cabbagge",
  "destructibles/grass",
  "destructibles/pot",
  "destructibles/rock",
  "destructibles/vase"
}

function destructible_meta:on_created()

  local destructible = self
  local dest_sprite_name = destructible:get_sprite():get_animation_set()

  if not destructible:get_treasure() then
    for _, name in pairs(item_drop_sprites) do
      if dest_sprite_name:match(name) then -- Check if the selected destructible sprite's name matches the sprite name.
        destructible:set_treasure("random")
      end
    end
  end
end

return true